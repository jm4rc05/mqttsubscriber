package br.ita.bditac.mqtt.subscriber;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttTestSubscriber implements MqttCallback {

    MqttClient myClient;

    MqttConnectOptions connOpt;

    static final String BROKER_URL = "tcp://iot.eclipse.org:1883";
    static final String MQTT_TOPIC = "br.ita.bditac/diagnostico";
    static final String MQTT_USERNAME = "<anonymous>";
    static final String MQTT_PASSWORD_MD5 = "<password>";

    public void connectionLost(Throwable t) {
        System.out.println("Connection lost!");
    }

    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public void messageArrived(String topic, MqttMessage message) {
        System.out.println("Message received: " + new String(message.getPayload()));
    }

    public void runClient() {

        String clientID = UUID.randomUUID().toString();
        connOpt = new MqttConnectOptions();

        connOpt.setCleanSession(true);
        connOpt.setKeepAliveInterval(30);
        connOpt.setUserName(MQTT_USERNAME);
        connOpt.setPassword(MQTT_PASSWORD_MD5.toCharArray());

        try {
            myClient = new MqttClient(BROKER_URL, clientID);
            myClient.setCallback(this);
            myClient.connect(connOpt);
        }
        catch (MqttException mex) {
        	System.out.println(mex.getMessage());
            mex.printStackTrace();
            
            System.exit(-1);
        }

        System.out.println("Connected to " + BROKER_URL);

        try {
            myClient.subscribe(MQTT_TOPIC, 0);

            try {
                Thread.sleep(4000);
            }
            catch (InterruptedException iex) {
            	System.out.println(iex.getMessage());
                iex.printStackTrace();
                
                System.exit(-1);
            }
        }
        catch (Exception ex) {
        	System.out.println(ex.getMessage());
            ex.printStackTrace();
            
            System.exit(-1);
        }

        try {
            myClient.disconnect();
        }
        catch (MqttException mex) {
        	System.out.println(mex.getMessage());
            mex.printStackTrace();
            
            System.exit(-1);
        }
    }

}
